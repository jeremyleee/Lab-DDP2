/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
import java.util.Scanner;
public class BingoCard {

	private Number[][] numbers;					//instance variable
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num) {				//membuat method marknum
		if (this.numberStates[num] == null) {
			return "Kartu tidak memiliki angka " + num;
		}
		if (this.numberStates[num].isChecked()) {
			return (num + " sebelumnya sudah tersilang" );
		} ;
		int baris=this.numberStates[num].getX();
		int kolom=this.numberStates[num].getY();
		this.numbers[baris][kolom].setChecked(true);
		this.numberStates[num].setChecked(true);
		this.isBingo=this.aBingo();
		return (num + " tersilang");
	}

	
	public String info() {						//membuat method info
		String KARTU = "";
		for (int p = 0; p < 5; p++) {
			KARTU=KARTU+"|";
			for (int w = 0; w < 5; w++) {
				if (numbers[p][w].isChecked()) {
					KARTU = KARTU + " X " + " |";
				} else {
					KARTU = KARTU +" "+ this.numbers[p][w].getValue() + " |";
				}

			}
			if (p!=4){KARTU+="\n";}
		}
		return (KARTU);
	}
	public void restart() {					//membuat method restart
		for (int c = 0; c < 5; c++) {
			for (int v = 0; v < 5; v++) {
				this.numbers[c][v].setChecked(false);
			}
		}
		for (int k = 0; k <= 99; k++) {
			if (this.numberStates[k] == null) continue;
			this.numberStates[k].setChecked(false);
		}

		System.out.println("Mulligan!");
	}
	public boolean aBingo(){				//membuat method aBingo
		boolean mantap;
		for(int i=0;i<5;i++){
			mantap = true;
			for(int j=0;j<5;j++){
				if(!this.numbers[i][j].isChecked()){
					mantap = false;
					break;
				}
			}
			if(mantap)return mantap;
		}

		for(int j=0;j<5;j++){
			mantap = true;
			for(int i=0;i<5;i++){
				if(!this.numbers[i][j].isChecked()){
					mantap = false;
					break;
				}
			}
			if(mantap)return mantap;
		}


		mantap = true;
		for(int i=0;i<5;i++){
			if(!this.numbers[i][i].isChecked()){
				mantap = false;break;
			}
		}
		if(mantap)return mantap;

		mantap= true;
		for(int i=0;i<5;i++){
			if(!this.numbers[i][4-i].isChecked()){
				mantap = false;
				break;
			}
		}
		return mantap;

	}
	}

