import java.util.Scanner;

public class BingoKuy {

    public static void main(String[] args) {        //membuat main
        Number[][] numbers = new Number[5][5];      //memberi pernyataan untuk Number[][]
        Number[] numberStates = new Number[100];
        Scanner input = new Scanner(System.in);

        System.out.println("BINGO! BANGO! BONGO! \nSelamat Datang di permainan Bingo!!! \nSilahkan masukan input:");
        for (int k = 0; k < 5; k++) {                   // membuat loop untuk input angka yang akan dimasukan ke array
            for (int q = 0; q < 5; q++) {
                int angka = input.nextInt();
                numbers[k][q] = new Number(angka, k , q);
                numberStates[angka] = numbers[k][q];

            }
        }
        BingoCard kartu = new BingoCard(numbers, numberStates);
        while (true) {
            String perintah = input.next();
            if (perintah.equals("RESTART")) {       //membuat kondisi perintah restart
                kartu.restart();
            } else if (perintah.equals("MARK")) {    //membuat kondisi perintah mark
                int marked = input.nextInt();
                System.out.println(kartu.markNum(marked));
            } else if (perintah.equals("INFO")) {        //membuat kondisi perintah info
                System.out.println(kartu.info());
            }
            else if (perintah.equals("STOP")){          //membuat kondisi perintah stop
                System.out.println("Terima Kasih");
                break;
            }
            else {                                      //membuat kondisi ketika perintah salah
                System.out.println("Perintah yang anda masukan tidak sesuai!\nContoh perintah yang bisa dimasukan: MARK,INFO,STOP,RESTART");
            }
            if(kartu.isBingo()){
                System.out.println("BINGO!!!");            //membuat kondisi perintah ketika mencapai BINGO
                System.out.println(kartu.info());
                break;}
        }

        }

    }