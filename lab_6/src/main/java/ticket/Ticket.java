package ticket;
import movie.Movie;

public class Ticket {
    private String hari;
    private int harga=60000;
    private boolean tipe;
    private String judul;
    private Movie movie;

    public Ticket(Movie movie,String hari,boolean tipe) {
        this.hari = hari;
        this.tipe = tipe ;
        this.movie = movie;
        this.harga = harga;
        if (hari.equals("Sabtu")||hari.equals("Minggu")){
            this.harga+=40000;
        }
        if (tipe==true){
            this.harga+=(20*harga/100);

        }
    }
    public String getHari(){
        return hari;
    }

    public boolean isTipe() {
        return tipe;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public int getHarga(){
        return harga;
    }
    public boolean getTipe(){
        return tipe;

    }

    public void setHari(String hari){
        this.hari=hari;
    }
    public void setHarga(int harga){
        this.harga=harga;
    }
    public void setTipe(boolean tipe){
        this.tipe=tipe;

    }
    public String a3d() {
        if (tipe == true) {
            return "3 Dimensi";
        } else {
            return "Biasa";
        }

    }
    /*
    public St Ticket(){
        if (hari.equals("Sabtu")||hari.equals("Minggu")){
            harga+=40000;
        }
        if (tipe==true){
            harga+=(20*harga/100);

        }
        System.out.println("------------------------------------------------------------------ \n" +
                "Film            : "+this.judul+"\n" +
                "Jadwal Tayang   : "+this.hari+"\n" +
                "Jenis           : "+a3d()+"\n" +
                "------------------------------------------------------------------\n");
        }
        */

    }