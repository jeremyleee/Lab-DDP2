package movie;
public class Movie {
    private String judul;
    private String genre;
    private int durasi;
    private String rating;
    private String jenis;
    public int UmurMin;

public Movie (String judul,String genre,int durasi,String rating,String jenis) {
    this.judul = judul;
    this.genre = genre;
    this.durasi = durasi;
    this.rating = rating;
    this.jenis = jenis;

}
public String getJudul(){
    return judul;
}
    public String getGenre(){
        return genre;
    }
    public String getRating(){
        return rating;
    }
    public String getJenis(){
        return jenis;
    }
    public int getDurasi(){
        return durasi;
    }
    public void setJudul(String judul){
        this.judul=judul;
    }
    public void setGenre(String genre){
        this.genre=genre;
    }
    public void setRating(String rating){
        this.rating=rating;
    }
    public void setDurasi(int durasi){
    this.durasi=durasi;
    }
    public void setJenis(String jenis){
    this.jenis=jenis;
    }
    public int getUmurMin(){
    return UmurMin;
    }

    public int MinUmur() {
        switch (this.genre) {
            case "Dewasa":
                return 17;
            case "Remaja":
                return 13;
            default:
                return 0;
        }
    }


        public String Movie(){
    return ("------------------------------------------------------------------ \n" +
            "Judul   : "+this.judul+"\n" +
            "Genre   : "+this.rating+"\n" +
            "Durasi  : "+this.durasi+" menit\n" +
            "Rating  : "+this.genre+"\n" +
            "Jenis   : "+this.jenis+"\n" +
            "------------------------------------------------------------------\n");
}

}