package theater;
import movie.Movie;
import ticket.Ticket;
import java.util.ArrayList;

public class Theater{
private String teater;
private int saldo;
Movie[] daftarfilm;
ArrayList<Ticket> daftarTicket = new ArrayList<Ticket>();
    public Theater(String teater,int saldo,ArrayList<Ticket> daftarTicket,Movie[] daftarfilm){
        this.teater=teater;
        this.saldo=saldo;
        this.daftarfilm=daftarfilm;
        this.daftarTicket=daftarTicket;
        }
    public int getSaldo(){
        return saldo;
    }
    public String getTeater(){
        return teater;
    }

    public Movie[] getDaftarfilm(){
        return daftarfilm;
    }
    public ArrayList<Ticket> getDaftarTicket(){
        return daftarTicket;
    }
    public void setSaldo(int saldo){
        this.saldo=saldo;
    }
    public void SetTeater(String teater){
        this.teater=teater;
    }
    public void setDaftarTicket(ArrayList<Ticket> daftarTicket){
        this.daftarTicket=daftarTicket;
    }
    public void setDaftarfilm(Movie[] daftarfilm){
        this.daftarfilm=daftarfilm;
    }
    public void printInfo() {
        System.out.print("------------------------------------------------------------------ \n" +
                "Bioskop                 : " + this.teater + "\n" +
                "Saldo Kas               : " + this.saldo + "\n" +
                "Jumlah tiket tersedia   : " + this.daftarTicket.size() + "\n");
        System.out.print("Daftar Film tersedia    : ");
        int k = daftarfilm.length;
        for (int x = 0; x < k; x++) {
            if (x == k - 1) {
                System.out.println(daftarfilm[x].getJudul());
            } else {
                System.out.print(daftarfilm[x].getJudul() + ",");
            }
        }
            System.out.println("------------------------------------------------------------------\n");

    }
public static void printTotalRevenueEarned(Theater[] listTheaters){
        int jumlahtotalpendapatan=0;
        for (Theater teater : listTheaters){
            jumlahtotalpendapatan+=teater.getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp."+jumlahtotalpendapatan+"\n" +
                "------------------------------------------------------------------\n");
        for (Theater teater:listTheaters){
            System.out.println("Bioskop         : "+teater.getTeater());
            System.out.println("Saldo Kast      : Rp." + teater.getSaldo() + "\n");
        }
    System.out.println("------------------------------------------------------------------");
}
}