package customer;
import ticket.Ticket;
import theater.Theater;
import movie.Movie;
import java.util.ArrayList;
import ticket.Ticket;

public class Customer {
    private String nama;
    private boolean kelamin;
    private int umur;

    public Customer(String nama, boolean kelamin, int umur) {
        this.nama = nama;
        this.kelamin = kelamin;
        this.umur = umur;
    }

    public void findMovie(Theater teater, String judul) {
        int count = 0;
        for (Movie movie : teater.getDaftarfilm()) {
            if (movie.getJudul().equals(judul)) {
                System.out.println("\n------------------------------------------------------------------");
                System.out.println("Judul\t: " + movie.getJudul() + "\nGenre\t: " + movie.getGenre());
                System.out.println("Durasi\t: " + movie.getDurasi() + " menit" + "\nRating\t: " + movie.getRating());
                System.out.println("Jenis\t: Film " + movie.getJenis());
                System.out.println("\n------------------------------------------------------------------");
                break;
            } else {
                count += 1;
            }
        }
        if (count == teater.getDaftarfilm().length) {
            System.out.println("Film " + judul + " yang dicari " + this.nama + " tidak ada di bioskop " + teater.getTeater());
        }
    }
    public Ticket orderTicket(Theater teater, String judul, String hari, String jenis) {
        int count = 0;
        Ticket beli = null;
        for (Ticket tiket : teater.getDaftarTicket()) {
            String aNama = tiket.getMovie().getJudul();
            String aHari = tiket.getHari();




            if (aNama.equals(judul) && aHari.equals(hari) && jenis.equals(jenis)) {
                if(this.umur >= tiket.getMovie().MinUmur()) {
                    System.out.println(this.nama + " telah membeli tiket " + aNama + " jenis " + jenis + " di " + teater.getTeater() + " pada hari " + tiket.getHari() + " seharga Rp. " + tiket.getHarga());
                    teater.setSaldo(teater.getSaldo() + tiket.getHarga());
                    beli = tiket;
                    break;
                } else {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " + tiket.getMovie().getJudul() + " dengan rating " + tiket.getMovie().getGenre());
                    break;
                }
            } else {
                count+=1;
            }
        }
        if (count == teater.getDaftarTicket().size()) {
            System.out.println("Tiket untuk film " +judul + " jenis " + jenis + " dengan jadwal " + hari + " tidak tersedia di " + teater.getTeater());
        }
        return beli;
    }
}

